﻿namespace Scribestar.CodeTest.Core
{
    public enum ScaleEnum
    {
        Hundred = 0,
        Thousand = 1,
        Million = 2,
        Billion = 3, //10^9
        Trillion = 4,//10^12
        Quadrillion = 5,//10^15
        Quintillion = 6, //10^18
        Sextillion = 7, //10^21
        Septillion = 8,
        Octillion = 9,
        Nonillion = 10,
        Decillion = 11,
        Undecillion = 12,
        Duodecillion = 13,
        Tredecillion = 14,
        Quattuordecillion = 15,
        Quindecillion = 16,
        Sexdecillion = 17,
        Septdecillion = 18,
        Octodecillion = 19,
        Novemdecillion = 20,
        Vigintillion = 21
    }

}
