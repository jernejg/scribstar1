﻿using System;
using System.Linq;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Rules;
using Scribestar.CodeTest.Core.Service;

namespace Scribestar.CodeTest.Core.enGB
{
    public class EnglishAssemblyLineBuilder : IAssemblyLineBuilder
    {
        private readonly IResources _resources;

        public EnglishAssemblyLineBuilder(IResources resources)
        {
            _resources = resources;
        }

        public IAssemblyLineContext Build()
        {
            Func<AssemblyLineContext, ThreeDigitGroupNumber, bool> isFirstGroup =
                (context, current) => context.Input.First() == current;

            Func<AssemblyLineContext, ThreeDigitGroupNumber, bool> untilLastNotBlank =
                (context, current) => context.Input.LastNonNullGroup().Position < current.Position;

            return new AssemblyLineContext()
                .ContinueWithIfCondition(new ZeroRuleProcessor(_resources), isFirstGroup)
                .ContinueWithIfCondition(new NegativeRuleProcessor(_resources), isFirstGroup)
                .ContinueWith(new HundredsRuleProcessor(_resources))
                .ContinueWith(new TensRuleProcessor(_resources))
                .ContinueWith(new RecombinationRuleProcessor(_resources))
                .ContinueWithIfCondition(new SeparatorProcessor(_resources), untilLastNotBlank)
                .AddPostProcessor(new RecombinationExceptionProcessor(_resources));
        } 
    }


}