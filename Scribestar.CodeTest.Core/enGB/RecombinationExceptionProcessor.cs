﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB
{
    public class RecombinationExceptionProcessor : IPostProcessor
    {
        private readonly IResources _resources;

        public RecombinationExceptionProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }

        public StringBuilder Proccess(IAssemblyLineContext context, StringBuilder result)
        {
            var isSpecialRecombinationRequired = new IsSpecialRecombinationRequired();

            if (isSpecialRecombinationRequired.IsSatisfiedBy(context.Input))
            {
                var tmpResult = result.ToString();
                var place = tmpResult.LastIndexOf(_resources.GetSeparator(), StringComparison.Ordinal);
                var newResult = tmpResult.Remove(place, _resources.GetSeparator().Length)
                                         .Insert(place, " " + _resources.GetStringResource("and"));
                
                result = new StringBuilder(newResult);
            }

            return result;

        }
    }
}