﻿using System;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Specifications
{
    public class IsSpecialHandlingOfTensRequired : ISpecification<ThreeDigitGroupNumber>
    {
        public Expression<Func<ThreeDigitGroupNumber, bool>> SatisfiedBy()
        {
            return number => (number.GetTens()) < 2;
        }
    }
}