﻿using System;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Specifications
{
    public class IsExactDivideBy100 : ISpecification<ThreeDigitGroupNumber>
    {
        public Expression<Func<ThreeDigitGroupNumber, bool>> SatisfiedBy()
        {
            return number => number.Value % 100 == 0;
        }
    }
}