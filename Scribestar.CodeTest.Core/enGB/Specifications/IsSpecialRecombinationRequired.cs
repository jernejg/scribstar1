﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Specifications
{
    public class IsSpecialRecombinationRequired : ISpecification<IEnumerable<ThreeDigitGroupNumber>>
    {
        /// <summary>
        /// One exception is when the final group does not include any hundreds 
        /// and there is more than one non-blank group. In this case the final comma is replaced with ‘and’ e.g. ‘one billion, one million and twelve’.
        /// </summary>
        /// <returns></returns>
        public Expression<Func<IEnumerable<ThreeDigitGroupNumber>, bool>> SatisfiedBy()
        {
            return numbers => new LastNonBlankGroupIncludesHundreds()
                                    .Not()
                                    .And(new IsMoreThanOneNoneBlankGroup())
                                    .IsSatisfiedBy(numbers);
        }
    }
}