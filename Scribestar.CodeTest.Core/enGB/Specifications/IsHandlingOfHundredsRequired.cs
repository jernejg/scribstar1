﻿using System;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Specifications
{
    public class IsHandlingOfHundredsRequired : ISpecification<ThreeDigitGroupNumber>
    {
        public Expression<Func<ThreeDigitGroupNumber, bool>> SatisfiedBy()
        {
            return number => (number.GetHundreds()) > 0;
        }
    }
}
