﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Specifications
{
    public class LastGroupIncludesHundreds : ISpecification<IEnumerable<ThreeDigitGroupNumber>>
    {
        public Expression<Func<IEnumerable<ThreeDigitGroupNumber>, bool>> SatisfiedBy()
        {
            return numbers => new IsHandlingOfHundredsRequired().IsSatisfiedBy(numbers.Last());
        }
    }
}