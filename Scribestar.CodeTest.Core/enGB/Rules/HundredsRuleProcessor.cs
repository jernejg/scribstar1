﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class HundredsRuleProcessor : IProcessor
    {
        private readonly IResources _resources;

        public HundredsRuleProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }

        /// <summary>
        /// If the hundreds portion of a three-digit group is not zero, the ThreeDigitGroupNumber of hundreds is added to the word
        /// If the three-digit group is exactly divisible by one hundred, the text ‘hundred’ is appended
        /// If not, the text ‘hundred and’ is appended e.g. ‘two hundred’ or ‘one hundred and twelve’
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string Proccess(ThreeDigitGroupNumber number)
        {
            var result = new StringBuilder();
            var shouldWriteHundreds = new IsHandlingOfHundredsRequired();
            var shouldWriteAnd = shouldWriteHundreds.AndNot(new IsExactDivideBy100());

            if (shouldWriteHundreds.IsSatisfiedBy(number))
            {
                result.Append(_resources.GetStringResource(number.GetHundreds())).Append(" ");
                result.Append(_resources.GetStringResource(100)).Append(" ");
            }

            if (shouldWriteAnd.IsSatisfiedBy(number))
                result.Append(_resources.GetStringResource("and")).Append(" ");

            return result.ToString();
        }
    }
}