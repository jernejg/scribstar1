﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class TensRuleProcessor : IProcessor
    {
        private readonly IResources _resources;

        public TensRuleProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }

        //1.)If the tens section of a three-digit group is two or higher the appropriate ‘-ty’ word (twenty, thirty, etc) is 
        //   added to the text and followed by the name of the third digit (unless the third digit is zero, which is ignored). 
        //2.)If the tens and the units are both zero, no text is added. 
        //3.)For any other value, the name of the one or two-digit ThreeDigitGroupNumber is added as a special case.
        public string Proccess(ThreeDigitGroupNumber number)
        {
            var result = new StringBuilder();

            var writeStandardTens = new IsHandlingOfTensRequired()
                .AndNot(new IsSpecialHandlingOfTensRequired());

            var writeStandardUnits = writeStandardTens
                .And(new IsHandlingOfUnitsRequired());

            var isSpecialCase = (new IsHandlingOfTensRequired()
                .Or(new IsHandlingOfUnitsRequired())
                .And(new IsSpecialHandlingOfTensRequired()));

            if (writeStandardTens.IsSatisfiedBy(number))
                result.Append(_resources.GetStringResource(number.GetTens() * 10))
                      .Append(" ");

            if (writeStandardUnits.IsSatisfiedBy(number))
                result.Append(_resources.GetStringResource(number.GetUnits()))
                      .Append(" ");
                    

            if (isSpecialCase.IsSatisfiedBy(number))
                result.Append(_resources.GetStringResource((number.GetTens() * 10) + number.GetUnits()))
                      .Append(" ");
            

            return result.ToString();
        }
    }
}
