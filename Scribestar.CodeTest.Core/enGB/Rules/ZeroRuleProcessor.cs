﻿using System;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class ZeroRuleProcessor : IProcessor
    {
        private readonly IResources _resources;
    

        public ZeroRuleProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }


        public string Proccess(ThreeDigitGroupNumber number)
        {
            var isZero = new IsBlank();

            return isZero.IsSatisfiedBy(number) ? _resources.GetStringResource(0) : string.Empty;
        }
    }
}