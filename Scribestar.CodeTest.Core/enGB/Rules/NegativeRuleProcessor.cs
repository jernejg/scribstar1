﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class NegativeRuleProcessor : IProcessor
    {
        private readonly IResources _resources;

        public NegativeRuleProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }

        public string Proccess(ThreeDigitGroupNumber number)
        {
            var result = new StringBuilder();
            var isPositive = new IsNegativeGroup().Not();

            if (isPositive.IsSatisfiedBy(number))
                return string.Empty;

            result.Append(_resources.GetStringResource("negative")).Append(" ");

            return result.ToString();
        }
    }
}