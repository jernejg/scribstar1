﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class RecombinationRuleProcessor : IProcessor
    {
        private readonly IResources _resources;

        public RecombinationRuleProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }
        /// <summary>
        /// 1. When recombining the translated three-digit groups, each group except the last is followed by a large number name and a comma, 
        /// 2. unless the group is blank and therefore not included at all. 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string Proccess(ThreeDigitGroupNumber number)
        {
            var result = new StringBuilder();

            var isBlankOrLastThreeDigitGroup = new IsBlank().Or(new IsLastThreeDigitGroup());

            if (isBlankOrLastThreeDigitGroup.IsSatisfiedBy(number))
                return String.Empty;

            return result
                .Append(_resources.GetStringResource(number.Position))
                .ToString();
        }
    }
}