﻿using System;
using System.Text;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.enGB.Rules
{
    public class SeparatorProcessor : IProcessor
    {
        private readonly IResources _resources;

        public SeparatorProcessor(IResources resources)
        {
            if (resources == null) throw new ArgumentNullException("resources");
            _resources = resources;
        }

        public string Proccess(ThreeDigitGroupNumber number)
        {
            var result = new StringBuilder();

            var isBlankOrLastThreeDigitGroup = new IsBlank().Or(new IsLastThreeDigitGroup());

            if (isBlankOrLastThreeDigitGroup.IsSatisfiedBy(number))
                return String.Empty;

            return result
                .Append(_resources.GetSeparator())
                .Append(" ").ToString();
        }
    }
}