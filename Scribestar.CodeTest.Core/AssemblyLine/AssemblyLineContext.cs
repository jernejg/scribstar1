﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scribestar.CodeTest.Core.AssemblyLine
{
    internal class AssemblyLineContext : IAssemblyLineContext
    {
        private readonly List<IProcessor> _processors;
        private readonly List<IPostProcessor> _postProcessors;
        private readonly List<ThreeDigitGroupNumber> _threeDigitNumbers;
        private readonly Dictionary<IProcessor, Func<AssemblyLineContext, ThreeDigitGroupNumber, bool>> _shouldProcessConditions;
        public StringBuilder Result { get; private set; }
        
        public IEnumerable<IProcessor> Processors 
        {
            get
            {
                return _processors.AsReadOnly();
            }
        }

        public IEnumerable<ThreeDigitGroupNumber> Input
        {
            get
            {
                return _threeDigitNumbers.AsReadOnly();
            }
        }

        public AssemblyLineContext()
        {
            _processors = new List<IProcessor>();
            _postProcessors = new List<IPostProcessor>();
            _threeDigitNumbers = new List<ThreeDigitGroupNumber>();
            _shouldProcessConditions = new Dictionary<IProcessor, Func<AssemblyLineContext, ThreeDigitGroupNumber, bool>>();
        }

        public IAssemblyLineContext SetNewInput(IEnumerable<ThreeDigitGroupNumber> numbers)
        {
            _threeDigitNumbers.Clear();
            _threeDigitNumbers.AddRange(numbers);

            return this;
        }

        public AssemblyLineContext AddPostProcessor(IPostProcessor postProcessor)
        {
            _postProcessors.Add(postProcessor);
            return this;
        }

        public AssemblyLineContext ContinueWith(IProcessor processor)
        {
            _processors.Add(processor);
            return this;
        }

        public AssemblyLineContext ContinueWithIfCondition(IProcessor processor,Func<AssemblyLineContext,ThreeDigitGroupNumber, bool> condition)
        {
           _shouldProcessConditions[processor] = condition;
            return ContinueWith(processor);
        }

        public IAssemblyLineContext Start()
        {
            Result = new StringBuilder();

            for (var i = 0; i < _threeDigitNumbers.Count; i++)
            {
                for (var j = 0; j < _processors.Count; j++)
                {
                    var shouldProcess = true;
                    Func<AssemblyLineContext, ThreeDigitGroupNumber, bool> condition;

                    if (_shouldProcessConditions.TryGetValue(_processors[j], out condition))
                        shouldProcess = condition(this, _threeDigitNumbers[i]);

                    if (shouldProcess)
                        Result.Append(_processors[j].Proccess(_threeDigitNumbers[i]));
                }

                if (_threeDigitNumbers[0].Value == 0)
                    break;
            }

            foreach (var postProcessor in _postProcessors)
            {
                Result = postProcessor.Proccess(this, Result);
            }

            return this;
        }
    }

    public static class AssemblyLineExtensions
    {
        
    }
}