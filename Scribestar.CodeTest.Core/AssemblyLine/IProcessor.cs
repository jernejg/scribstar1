﻿namespace Scribestar.CodeTest.Core.AssemblyLine
{
    public interface IProcessor
    {
        string Proccess(ThreeDigitGroupNumber number);
    }
}
