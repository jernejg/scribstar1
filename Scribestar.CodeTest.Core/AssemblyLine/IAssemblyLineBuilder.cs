﻿namespace Scribestar.CodeTest.Core.AssemblyLine
{
    public interface IAssemblyLineBuilder
    {
        IAssemblyLineContext Build();
    }
}