﻿using System.Text;

namespace Scribestar.CodeTest.Core.AssemblyLine
{
    public interface IPostProcessor
    {
        StringBuilder Proccess(IAssemblyLineContext context, StringBuilder result);
    }
}