﻿using System.Collections.Generic;
using System.Text;

namespace Scribestar.CodeTest.Core.AssemblyLine
{
    public interface IAssemblyLineContext
    {
        StringBuilder Result { get; }
        IEnumerable<IProcessor> Processors { get; }
        IEnumerable<ThreeDigitGroupNumber> Input { get; }
        IAssemblyLineContext SetNewInput(IEnumerable<ThreeDigitGroupNumber> numbers);
        IAssemblyLineContext Start();
    }
}