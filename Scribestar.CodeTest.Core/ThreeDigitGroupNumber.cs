﻿using System;

namespace Scribestar.CodeTest.Core
{
    public class ThreeDigitGroupNumber
    {
        public short Value { get; private set; }
        public ScaleEnum Position { get; private set; }

        public ThreeDigitGroupNumber(short value, ScaleEnum position)
        {
            Value = value;
            Position = position;
        }

        public ushort GetHundreds()
        {
            return (ushort)Math.Abs((Value / 100));
        }

        public ushort GetTens()
        {
            return (ushort)Math.Abs((Value % 100) / 10);
        }

        public ushort GetUnits()
        {
            return (ushort)Math.Abs((Value % 100) % 10);

        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}