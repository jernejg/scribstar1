﻿using Scribestar.CodeTest.Core.AssemblyLine;

namespace Scribestar.CodeTest.Core.Service
{
    public interface IResultController
    {
        IAssemblyLineContext Prepare(string inputArg0);
    }
}