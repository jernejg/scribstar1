﻿using System.Globalization;

namespace Scribestar.CodeTest.Core.Service
{
    public class Resources : IResources
    {
        private readonly string _culureName;

        public Resources(string culureName)
        {
            _culureName = culureName;
        }

        public string GetStringResource(ScaleEnum position)
        {
            return GetStringResource("p" + (int)position);
        }

        public string GetStringResource(string key)
        {
            return Properties.Resources.ResourceManager.GetString(key, CultureInfo.GetCultureInfo(_culureName));
        }

        public string GetStringResource(int key)
        {
            return GetStringResource("i" + key);
        }

        public string GetStringResource(int key, CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

        public string GetSeparator()
        {
            return GetStringResource("separator");
        }
    }
}