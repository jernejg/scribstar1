﻿namespace Scribestar.CodeTest.Infrastructure
{
    public interface IWriter
    {
        void Write(string message, params object[] formattingArguments);
        void WriteError(string message, params object[] formattingArguments);
    }
}