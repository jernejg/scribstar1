﻿using System.Globalization;

namespace Scribestar.CodeTest.Core.Service
{
    public interface IResources
    {
        string GetStringResource(int key);
        string GetStringResource(ScaleEnum position);
        string GetStringResource(string key);
        string GetStringResource(int key, CultureInfo culture);
        string GetSeparator();
    }
}
