﻿using System;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.Service
{
    public class ResultController : IResultController
    {
        private readonly IAssemblyLineBuilder _assemblyLineBuilder;
        private readonly ISpecification<string> _validator;
        private readonly IThreeDigitGroupNumberParser _parser;

        public ResultController(IAssemblyLineBuilder assemblyLineBuilder,ISpecification<string> validator,IThreeDigitGroupNumberParser parser)
        {
            if (assemblyLineBuilder == null) throw new ArgumentNullException("assemblyLineBuilder");
            if (validator == null) throw new ArgumentNullException("validator");
            if (parser == null) throw new ArgumentNullException("parser");

            _assemblyLineBuilder = assemblyLineBuilder;
            _validator = validator;
            _parser = parser;
        }

        public IAssemblyLineContext Prepare(string inputArg0)
        {
            if (!_validator.IsSatisfiedBy(inputArg0))
                throw new FormatException("Input must be numeric!");

            var input = _parser.Parse(inputArg0);

            var assemblyLine = _assemblyLineBuilder.Build();

            return assemblyLine
                .SetNewInput(input)
                .Start();
        }
    }
}