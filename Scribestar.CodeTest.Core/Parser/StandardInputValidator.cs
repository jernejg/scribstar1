﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Scribestar.CodeTest.Core.SpecificationFramework;

namespace Scribestar.CodeTest.Core.Parser
{
    public class StandardInputValidator : ISpecification<string>
    {
        public Expression<Func<string, bool>> SatisfiedBy()
        {
            return s =>!String.IsNullOrEmpty(s) 
                && s.Skip(1).All(x => (Char.IsDigit(x))) 
                && (s.ElementAt(0).CompareTo('-') == 0 || Char.IsDigit(s.ElementAt(0)))
                && s.Length<=66;
        }
    }
}