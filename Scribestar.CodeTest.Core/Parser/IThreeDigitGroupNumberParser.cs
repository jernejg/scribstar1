﻿using System.Collections.Generic;

namespace Scribestar.CodeTest.Core.Parser
{
    public interface IThreeDigitGroupNumberParser
    {
        IEnumerable<ThreeDigitGroupNumber> Parse(string value);
    }
}