﻿using System.Collections.Generic;
using System.Linq;

namespace Scribestar.CodeTest.Core.Parser
{
    public class ThreeDigitGroupNumberParser : IThreeDigitGroupNumberParser
    {
        public IEnumerable<ThreeDigitGroupNumber> Parse(string value)
        {
            var sign = +1;
            var skip = 0;

            if (value.ElementAt(0).CompareTo('-') == 0)
            {
                sign = -1;
                skip = 1;
            }

            var threeDigitGroupNumbers =  value
                .Skip(skip)
                .Reverse()
                .Slice(3)
                .Select((x, i) => new ThreeDigitGroupNumber(short.Parse(new string(x.Reverse().ToArray())), (ScaleEnum) i))
                .Reverse();

            var first = threeDigitGroupNumbers.First();

            if (sign == -1)
                first = new ThreeDigitGroupNumber((short) (first.Value*sign), first.Position);

            yield return first;

            foreach (var element in threeDigitGroupNumbers.Skip(1))
                yield return element;
        }
    }
}