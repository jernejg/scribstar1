﻿using System.Collections.Generic;
using System.Linq;

namespace Scribestar.CodeTest.Core
{
    public static class Helper
    {
        public static IEnumerable<IEnumerable<T>> Slice<T>(this IEnumerable<T> sequence, int maxItemsPerSlice)
        {
            var slice = new List<T>(maxItemsPerSlice);

            foreach (var item in sequence)
            {
                slice.Add(item);

                if (slice.Count == maxItemsPerSlice)
                {
                    yield return slice.ToArray();
                    slice.Clear();
                }
            }

            // partial slice
            if (slice.Count > 0)
                yield return slice.ToArray();
        }

        public static ThreeDigitGroupNumber LastNonNullGroup(this IEnumerable<ThreeDigitGroupNumber> sequence)
        {
            return sequence.Reverse().First(x=>x.Value > 0);
        }
    }
}
