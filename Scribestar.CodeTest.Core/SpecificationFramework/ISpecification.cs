﻿using System;
using System.Linq.Expressions;

namespace Scribestar.CodeTest.Core.SpecificationFramework
{
    /// <summary>
    ///     Base contract for Specification pattern
    ///     This is really a variant implementation, added Linq and
    ///     lambda expression into this pattern.
    /// </summary>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public interface ISpecification<TEntity> where TEntity : class
    {
        /// <summary>
        ///     Check if this specification is satisfied by a
        ///     specific expression lambda
        /// </summary>
        /// <returns></returns>
        Expression<Func<TEntity, bool>> SatisfiedBy();
    }
}