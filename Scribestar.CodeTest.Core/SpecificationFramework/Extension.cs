﻿namespace Scribestar.CodeTest.Core.SpecificationFramework
{
    public static class Extension
    {
        public static bool IsSatisfiedBy<T>(this ISpecification<T> me, T entity) where T:class
        {
            return me.SatisfiedBy().Compile()(entity);
        }

        public static ISpecification<T> And<T>(this ISpecification<T> left, ISpecification<T> right) where T : class
        {
            return new AndSpecification<T>(left, right);
        }


        public static ISpecification<T> Or<T>(this ISpecification<T> left, ISpecification<T> right) where T : class
        {
            return new OrSpecification<T>(left, right);
        }

        public static ISpecification<T> Not<T>(this ISpecification<T> me) where T : class
        {
            return new NotSpecification<T>(me);
        }

        public static ISpecification<T> AndNot<T>(this ISpecification<T> left, ISpecification<T> right) where T : class
        {
            return new AndSpecification<T>(left, new NotSpecification<T>(right));
        }
    }
}
