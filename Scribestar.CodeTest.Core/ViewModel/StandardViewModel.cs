﻿using System;
using Scribestar.CodeTest.Core.AssemblyLine;

namespace Scribestar.CodeTest.Core.ViewModel
{
    public class StandardViewModel : IViewModel
    {
        public string NumberAsWord 
        {
            get
            {
                return AssemblyLineInfo.Result.ToString();
            }
        }

        public IAssemblyLineContext AssemblyLineInfo { get; set; }
    }
}