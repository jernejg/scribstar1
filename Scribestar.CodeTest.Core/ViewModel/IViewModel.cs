﻿using Scribestar.CodeTest.Core.AssemblyLine;

namespace Scribestar.CodeTest.Core.ViewModel
{

    public interface IViewModel
    {
        string NumberAsWord { get; }
        IAssemblyLineContext AssemblyLineInfo { get; set; }


    }
}