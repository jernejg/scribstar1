﻿using System.Linq;
using Moq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.enGB.Rules;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Tests.Infrastructure;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Rules
{
    public class Tens_Rules_Processor_Test
    {
        [Theory]
        [InlineAutoMoqData("1",true)]
        [InlineAutoMoqData("12",true)]
        [InlineAutoMoqData("21",false)]
        public void Tens_Rule_Special_Case_Test(
            string digitGroup,
            bool shouldCallOnce,
            [Frozen]Mock<IResources> resourceMock,
            TensRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();

            sut.Proccess(input);

            resourceMock.Verify(x => x.GetStringResource((input.GetTens() * 10) + input.GetUnits()), shouldCallOnce ? Times.Once(): Times.Never());
        }

        [Theory]
        [InlineAutoMoqData("20")]
        [InlineAutoMoqData("30")]
        [InlineAutoMoqData("40")]
        public void Standard_Tens_Test_Without_Units(
            string digitGroup,
            [Frozen]Mock<IResources> resourceMock,
            TensRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();

            sut.Proccess(input);

            resourceMock.Verify(x => x.GetStringResource((input.GetTens() * 10)), Times.Once());

        }

        [Theory]
        [InlineAutoMoqData("21")]
        [InlineAutoMoqData("32")]
        [InlineAutoMoqData("43")]
        public void Standard_Tens_Test_With_Units(
            string digitGroup,
            [Frozen]Mock<IResources> resourceMock,
            TensRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();
            var units = input.GetUnits();
            sut.Proccess(input);

            resourceMock.Verify(x => x.GetStringResource((input.GetTens() * 10)), Times.Once());
            resourceMock.Verify(x => x.GetStringResource(units), Times.Once());
        } 
    }
}