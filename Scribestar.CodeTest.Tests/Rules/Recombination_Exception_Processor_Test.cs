﻿using System.Text;
using Moq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.AssemblyLine;
using Scribestar.CodeTest.Core.enGB;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Tests.Infrastructure;
using Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Rules
{
    public class Recombination_Exception_Processor_Test
    {
        [Theory]
        [InlineAutoMoqData("1000000012", true)]
        [InlineAutoMoqData("1000000112", false)]
        public void Replace_last_comma_with_and_if_exception(
            string input,
            bool exceptionExists,
            [Frozen]Mock<IResources> resourceMock,
            [Frozen]Mock<IAssemblyLineContext> context,
            RecombinationExceptionProcessor sut,
            ThreeDigitGroupNumberParser parser
            )
        {
            var inputData = parser.Parse(input);

            context.Setup(x => x.Input).Returns(inputData);
            resourceMock.Setup(x => x.GetSeparator()).Returns(",");
            resourceMock.Setup(x => x.GetStringResource("and")).Returns("and");

            var result = sut.Proccess(context.Object, new StringBuilder(","));

            Assert.Equal(exceptionExists,System.String.Compare(result.ToString(), " and", System.StringComparison.Ordinal) ==0);


        } 
    }
}