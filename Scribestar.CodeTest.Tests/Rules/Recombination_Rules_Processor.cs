﻿using System.Linq;
using Moq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.enGB.Rules;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Tests.Infrastructure;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Rules
{
    public class Recombination_Rules_Processor
    {
        [Theory]
        [InlineAutoMoqData("100000", true)]
        [InlineAutoMoqData("0", false)]
        [InlineAutoMoqData("000100000", false)]
        public void Insert_Large_Name_And_A_Comma_Test(
            string input,
            bool shouldInsertLargeName,
            [Frozen]Mock<IResources> resourceMock,
            RecombinationRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var threeDigitGroupNumber = parser.Parse(input).First();
            sut.Proccess(threeDigitGroupNumber);

            resourceMock.Verify(x => x.GetStringResource(threeDigitGroupNumber.Position), shouldInsertLargeName ? Times.Once() : Times.Never());
        }
    }
}