﻿using System.Linq;
using Moq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.enGB.Rules;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Tests.Infrastructure;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Rules
{
    public class Hundreds_Rules_Processor_Test
    {
        [Theory]
        [InlineAutoMoqData("100", true,false)]
        [InlineAutoMoqData("101", true,true)]
        [InlineAutoMoqData("99", false,false)]
        public void Hundreds_Rule_Processor_Test(
            string digitGroup,
            bool shouldProcessHundreds,
            bool shouldWriteAnd,
            [Frozen]Mock<IResources> resourceMock,
            HundredsRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();
            var hundreds = input.GetHundreds();

            sut.Proccess(input);

            resourceMock.Verify(x => x.GetStringResource(hundreds), shouldProcessHundreds ? Times.Once() : Times.Never());
            resourceMock.Verify(x => x.GetStringResource(100), shouldProcessHundreds ? Times.Once() : Times.Never());
            resourceMock.Verify(x => x.GetStringResource("and"), shouldWriteAnd ? Times.Once() : Times.Never());

        }
    }
}