﻿using System.Linq;
using Moq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.enGB.Rules;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Tests.Infrastructure;
using Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Rules
{
    public class English_Rules_Test
    {
        [Theory]
        [InlineAutoMoqData("0", true)]
        [InlineAutoMoqData("1", false)]
        public void Zero_Rule_Gets_0_From_Resource_If_DigitGroup_IsZero(
            string digitGroup,
            bool expectedGetZeroFromResource,
            [Frozen]Mock<IResources> resourceMock,
            ZeroRuleProcessor sut,
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();

            sut.Proccess(input);

            resourceMock.Verify(x => x.GetStringResource(0), expectedGetZeroFromResource ? Times.Once() : Times.Never());
        }

        [Theory]
        [InlineAutoMoqData("1", false,"")]
        [InlineAutoMoqData("-1", true,"negative ")]
        public void Negative_Rule_Processor_Test(
            string digitGroup, 
            bool expectedNegativeFromResource, 
            string expectedResult,
            [Frozen]Mock<IResources> resourceMock, 
            NegativeRuleProcessor sut, 
            ThreeDigitGroupNumberParser parser)
        {
            var input = parser.Parse(digitGroup).First();

            resourceMock.Setup(x => x.GetStringResource("negative")).Returns("negative");

            Assert.Equal(expectedResult, sut.Proccess(input));

            resourceMock.Verify(x => x.GetStringResource("negative"), expectedNegativeFromResource ? Times.Once() : Times.Never());

        }



    }
}