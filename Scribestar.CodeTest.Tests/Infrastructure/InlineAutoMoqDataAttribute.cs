﻿using Ploeh.AutoFixture.Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests.Infrastructure
{
    internal class InlineAutoMoqDataAttribute : CompositeDataAttribute
    {
        internal InlineAutoMoqDataAttribute(params object[] values)
            : base(new DataAttribute[] { 
                new InlineDataAttribute(values), 
                new AutoMoqDataAttribute() })
        {
        }
    }


}