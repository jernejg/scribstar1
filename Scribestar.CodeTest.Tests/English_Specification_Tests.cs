﻿using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core;
using Scribestar.CodeTest.Core.enGB.Specifications;
using Scribestar.CodeTest.Core.SpecificationFramework;
using Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests
{
    public class English_Specification_Tests
    {
        [Theory]
        [InlineAutoData(0, true)]
        [InlineAutoData(100, false)]
        [InlineAutoData(10, false)]
        [InlineAutoData(1, false)]
        public void Three_Digit_Group_Is_Zero(int groupValue, bool expectedResult, IsBlank sut)
        {
            var actualResult = sut.IsSatisfiedBy(new ThreeDigitGroupNumber((short)groupValue, (ScaleEnum)new Fixture().Create<int>()));

            Assert.Equal(expectedResult, actualResult);
        }

        /// <summary>
        /// One exception is when the final group does not include any hundreds and there is more than one non-blank group. 
        /// </summary>
        /// <param name="sut"></param>
        [Theory, AutoData]
        public void Recombination_Rule_Special_Treatment(IsSpecialRecombinationRequired sut)
        {
            var actual = sut.IsSatisfiedBy(new[]
            {
                new ThreeDigitGroupNumber(1, ScaleEnum.Billion),
                new ThreeDigitGroupNumber(1, ScaleEnum.Million),
                new ThreeDigitGroupNumber(0, ScaleEnum.Thousand),
                new ThreeDigitGroupNumber(12, ScaleEnum.Hundred)
            });

            Assert.Equal(true, actual);
        }

        [Theory, AutoData]
        public void Last_Group_Includes_Hundreds(LastGroupIncludesHundreds sut)
        {
            var actual = sut.IsSatisfiedBy(new[]
            {
                new ThreeDigitGroupNumber(1, ScaleEnum.Billion),
                new ThreeDigitGroupNumber(1, ScaleEnum.Million),
                new ThreeDigitGroupNumber(0, ScaleEnum.Thousand),
                new ThreeDigitGroupNumber(12, ScaleEnum.Hundred)
            });

            Assert.Equal(false, actual);
        }

        [Theory, AutoData]
        public void Is_More_Than_One_Blank_Group(IsMoreThanOneNoneBlankGroup sut)
        {
            var actual = sut.IsSatisfiedBy(new[]
            {
                new ThreeDigitGroupNumber(1, ScaleEnum.Billion),
                new ThreeDigitGroupNumber(1, ScaleEnum.Million),
                new ThreeDigitGroupNumber(0, ScaleEnum.Thousand),
                new ThreeDigitGroupNumber(12, ScaleEnum.Hundred)
            });


            Assert.Equal(true, actual);
        }
    }
}
