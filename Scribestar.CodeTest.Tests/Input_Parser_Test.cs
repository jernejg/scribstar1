﻿using System.Linq;
using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core;
using Scribestar.CodeTest.Core.Parser;
using Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests
{
    /// <summary>
    /// Test the parser that converts input "123456" to IEnumerable<ThreeDigitGroupNumber>
    /// </summary>
    public class Input_Parser_Test
    {
        [Theory]
        [InlineAutoData("-1", 1)]
        [InlineAutoData("1", 1)]
        [InlineAutoData("0", 1)]
        [InlineAutoData("12", 1)]
        [InlineAutoData("123", 1)]
        [InlineAutoData("1234", 2)]
        [InlineAutoData("123456", 2)]
        [InlineAutoData("-123456", 2)]
        [InlineAutoData("1234567", 3)]
        public void Number_Of_Groups_Returned_From_Parser(string input, int expectedGroupCount, ThreeDigitGroupNumberParser sut)
        {
            var actualGroupCount = sut.Parse(input).Count();

            Assert.Equal(expectedGroupCount, actualGroupCount);
        }

        [Theory]
        [InlineAutoData("-1", true)]
        [InlineAutoData("1", false)]
        [InlineAutoData("-1234", true)]
        public void Negative_Sign_Is_Applied_To_First_Group(string input, bool isFirstGroupNegativeExpected, ThreeDigitGroupNumberParser sut)
        {
            var isFirstGroupNegativeActual = sut.Parse(input).First().Value < 0;

            Assert.Equal(isFirstGroupNegativeExpected, isFirstGroupNegativeActual);
        }

        [Theory]
        [InlineAutoData("-1", -1)]
        [InlineAutoData("1", 1)]
        [InlineAutoData("-1234", -1)]
        public void First_Group_Value_After_Negative_Value_Is_Applied(string input, int isFirstGroupValueExpected, ThreeDigitGroupNumberParser sut)
        {
            var firstGroupValueActual = sut.Parse(input).First().Value;

            Assert.Equal(isFirstGroupValueExpected, firstGroupValueActual);
        }

        [Theory]
        [InlineAutoData("1", 1)]
        [InlineAutoData("12", 12)]
        [InlineAutoData("123", 123)]
        [InlineAutoData("1234", 234)]
        [InlineAutoData("12345", 345)]
        [InlineAutoData("123456", 456)]
        public void Last_Group_Value_Test(string input, int expectedLastGroupValue, ThreeDigitGroupNumberParser sut)
        {
            var actualLastGroupValue = sut.Parse(input).Last().Value;

            Assert.Equal(expectedLastGroupValue, actualLastGroupValue);
        }

        [Theory]
        [InlineAutoData("123",ScaleEnum.Hundred)]
        [InlineAutoData("0",ScaleEnum.Hundred)]
        [InlineAutoData("1234",ScaleEnum.Thousand)]
        [InlineAutoData("1234567",ScaleEnum.Million)]
        public void Correct_Position_Scale_Is_Applied_To_First_Element(string input,ScaleEnum expectedScaleEnum, ThreeDigitGroupNumberParser sut)
        {
            var actualScaleEnum = sut.Parse(input).First().Position;

            Assert.Equal(expectedScaleEnum, actualScaleEnum);
            
        }
    }
}