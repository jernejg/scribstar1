﻿using Ploeh.AutoFixture.Xunit;
using Scribestar.CodeTest.Core.Parser;
using Scribestar.CodeTest.Core.SpecificationFramework;
using Xunit;
using Xunit.Extensions;

namespace Scribestar.CodeTest.Tests
{
    /// <summary>
    /// Test the input validator that checks the string 
    /// before passing it to parser to extract three digit groups
    /// </summary>
    public class Input_Validation_Test
    {
        [Theory]
        [InlineAutoData("", false)]
        [InlineAutoData(" ", false)]
        [InlineAutoData("   ", false)]
        [InlineAutoData("-   ", false)]
        [InlineAutoData("1-", false)]
        [InlineAutoData("1234p", false)]
        [InlineAutoData(".123", false)]
        [InlineAutoData("-1", true)]
        [InlineAutoData("1", true)]
        [InlineAutoData("1", true)]
        [InlineAutoData("100000000000000000000000000000000000000000000000000000000000000000", true)]
        [InlineAutoData("1000000000000000000000000000000000000000000000000000000000000000000", false)]
        public void Standard_Input_Validation(string input,bool expectedIsValidString,StandardInputValidator sut)
        {
            var actualIsValidString = sut.IsSatisfiedBy(input);

            Assert.Equal(expectedIsValidString, actualIsValidString);
            
        }
    }
}