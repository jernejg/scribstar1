﻿using StructureMap.Configuration.DSL;
using StructureMap.Graph;

namespace Scribestar.CodeTest.Program.Configuration
{
    public class ConsoleModule : Registry
    {
        public ConsoleModule()
        {
            Scan(s =>
            {
                s.WithDefaultConventions();
                s.AssembliesFromApplicationBaseDirectory(assembly => assembly.GetName().Name == "Scribestar.CodeTest.Core");
                s.AssembliesFromApplicationBaseDirectory(assembly => assembly.GetName().Name == "Scribestar.CodeTest.Program");
                s.IncludeNamespace("Scribestar.CodeTest.Core.Service");
                s.IncludeNamespace("Scribestar.CodeTest.Program");
                s.IncludeNamespace("Scribestar.CodeTest.Core.ViewModel");
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });
        }
    }
}