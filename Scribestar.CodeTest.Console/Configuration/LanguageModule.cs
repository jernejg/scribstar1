﻿using Scribestar.CodeTest.Core.Service;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;

namespace Scribestar.CodeTest.Program.Configuration
{
    public class LanguageModule : Registry
    {
        public LanguageModule()
        {
            Scan(s =>
            {
                s.AssembliesFromApplicationBaseDirectory(assembly => assembly.GetName().Name == "Scribestar.CodeTest.Core");
                s.IncludeNamespace("Scribestar.CodeTest.Core.enGB");
                For<IResources>().Singleton().Use<Resources>().Ctor<string>().Is("en-GB");
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });
        }
    }
}