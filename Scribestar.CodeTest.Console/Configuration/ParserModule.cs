﻿using StructureMap.Configuration.DSL;
using StructureMap.Graph;

namespace Scribestar.CodeTest.Program.Configuration
{
    public class ParserModule : Registry
    {
        public ParserModule()
        {
            Scan(s =>
            {
                s.AssembliesFromApplicationBaseDirectory(assembly => assembly.GetName().Name == "Scribestar.CodeTest.Core");
                s.IncludeNamespace("Scribestar.CodeTest.Core.Parser");
                s.RegisterConcreteTypesAgainstTheFirstInterface();
            });
        } 
    }
}