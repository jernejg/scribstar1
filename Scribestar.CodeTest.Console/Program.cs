﻿using System;
using System.Globalization;
using Scribestar.CodeTest.Core.Service;
using Scribestar.CodeTest.Core.ViewModel;
using Scribestar.CodeTest.Infrastructure;
using Scribestar.CodeTest.Program.Configuration;
using StructureMap;
using StructureMap.Pipeline;

namespace Scribestar.CodeTest.Program
{
    public class Program
    {
        private readonly string _inputArg0;
        private readonly IResultController _resultController;
        private readonly IWriter _writer;
        private readonly IViewModel _viewModel;

        private enum StatusCode
        {
            Failure = -1,
            Success = 1,
            Unknown = 0
        }

        public Program(string inputArg0,IResultController resultController,IWriter writer,IViewModel viewModel)
        {
            if (resultController == null) throw new ArgumentNullException("resultController");
            if (writer == null) throw new ArgumentNullException("writer");

            _inputArg0 = inputArg0;
            _resultController = resultController;
            _writer = writer;
            _viewModel = viewModel;
        }

        static int Main(string[] args)
        {
            using (var container = Bootstrap())
            {
                var inputNumber = (args.Length == 0 ? "0" : args[0]);
                var runner = container.GetInstance<Program>(new ExplicitArguments().Set(inputNumber));

                if (runner.Run())
                    return (int)StatusCode.Success;

                return (int)StatusCode.Failure;
            }
        }

        public bool Run()
        {
            var exitStatusSuccess = true;

            try
            {
                _viewModel.AssemblyLineInfo = _resultController.Prepare(_inputArg0);
                Console.WriteLine(_viewModel.NumberAsWord);
            }
            catch (FormatException e)
            {
                _writer.WriteError(e.Message);
                exitStatusSuccess = false;
            }
            catch (Exception e)
            {
                _writer.WriteError(e.Message);
                exitStatusSuccess = false;
            }

            return exitStatusSuccess;
        }

        private static IContainer Bootstrap()
        {
            return new Container(c =>
            {
                c.AddRegistry(new ConsoleModule());
                c.AddRegistry(new ParserModule());
                c.AddRegistry(new LanguageModule());
            });
        }
    }
}
