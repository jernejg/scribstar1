﻿using System;
using Scribestar.CodeTest.Infrastructure;

namespace Scribestar.CodeTest.Program
{
    public class Writer : IWriter
    {
        public void Write(string message, params object[] formattingArguments)
        {
            Write(message, ConsoleColor.White, formattingArguments);
        }

        public void WriteError(string message, params object[] formattingArguments)
        {
            Write(message, ConsoleColor.Red, formattingArguments);
        }

        private void Write(string message, ConsoleColor color = ConsoleColor.White, params object[] formattingArguments)
        {
            var originalColor = Console.ForegroundColor;

            try
            {
                Console.ForegroundColor = color;
                Console.WriteLine(message, formattingArguments);
            }
            finally
            {
                Console.ForegroundColor = originalColor;
            }
        }
    }
}