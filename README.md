# README #

Libraries used: Structuremap, Autofixture,Xunit

### Patterns used ###

* Specification
* DI
* Fluent interface for combining rules into an assembly line

### Specification pattern ###

* It helps to model the rules defined in the Code Test Document so that the gap between the code solution and problem domain is as little as possible.
* It reduces cyclomatic complexity since there is almost no nesting of control structures.
* Very easy to test

DOCUMENT: "One exception is when the final group does not include any hundreds and there is more than one non-blank group."
![Untitled picture.png](https://bitbucket.org/repo/Gyy7qa/images/1766570946-Untitled%20picture.png)

### Assembly line ###

* The digit to word translation is modeled into an assembly line that takes three digit number groups as input and outputs the word representation. The input string can be up to 66 chars long (Vigintillion 10^63).
* You can combine different rules together, add conditions and post process operations.

![assembly.png](https://bitbucket.org/repo/Gyy7qa/images/930371328-assembly.png)